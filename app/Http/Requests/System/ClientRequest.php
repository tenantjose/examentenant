<?php

namespace App\Http\Requests\System;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'email' => ['required', 'email'],
            'documento' => ['required', 'unique:clients,documento,' . $id],
            'name_customer' => ['required', 'unique:clients,name_customer,' . $id],
            'password'=>['required'],
            'subdominio'=>['required']
        ];
    }
}
