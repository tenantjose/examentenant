<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\System\clients;
use App\Http\Requests\System\ClientRequest;
use Exception;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Illuminate\Support\Facades\DB;
use Hyn\Tenancy\Environment;
use App\Http\Resources\System\ClientsCollections;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        /*Utilizamos el strlower para convertir todo a minusculas para un mejor manejo y lo almacenamos en una variable*/
        $subdominio = strtolower($request->subdominio);
        /** El uuid es la variable que se registrará en el la tabla website para identificar los bases y para la creación de ella misma, he colocado una prefijo que es
         * tenant para identificar que las bases que empiezan con tenant son las de clientes.
         */
        $uuid = 'tenant_' . $subdominio;
        /**Capturamos la url base predeterminana del vendor y con la varible del subdominio realizamos la creación del fqdn el cual es la hostname del cliente para que
         * pueda acceder al aplicativo.
         */
        $fqdn = $subdominio . '.' . config('tenant.app_url_base');
        /**Instanciamos el website y el hostname */
        $website = new Website();
        $hostname = new Hostname();

        /**Validamos si existe el subdominio ingresado */
        $this->validar_tenant($website, $uuid);
        /**Utilizamos trans sql */
        $token = str_random(50);
        DB::connection('system')->beginTransaction();
        try {
            /**pasamos los datos captrados a las variabes del hostname y del website*/
            $website->uuid = $uuid;
            $hostname->fqdn = $fqdn;
            /**Utilizamos los repositorios de tenant para poder crear sitios web y host  */
            app(WebsiteRepository::class)->create($website);
            app(HostnameRepository::class)->attach($hostname, $website);
            /**para poder crear el usuario administrador de los clientes , tenemos que cambiar la base de datos entonces utilizamos el environment de tenacy para
             * cambiar de base
             */
            $tenancy = app(Environment::class);
            $tenancy->tenant($website);
            /**Creamos el cliente*/
            $client = new clients();
            $client->hostname_id = $hostname->id;
            $client->documento = $request->documento;
            $client->name_customer = $request->name_customer;
            $client->email = $request->email;
            $client->save();
            /**Si todo se crea correctamente enviamos todo para que registre */

            DB::connection('system')->commit();
        } catch (Exception $e) {
            /**en caso que surga un error eliminamos los registros */
            DB::connection('system')->rollBack();
            app(HostnameRepository::class)->delete($hostname, true);
            app(WebsiteRepository::class)->delete($website, true);

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        DB::connection('tenant')->table('users')->insert([
            'name' => 'Administrador',
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'api_token' => $token,
            'role' => 'admin'
        ]);

        return [
            'success' => true,
            'message' => 'Se registro el cliente ya puede acceder a ' . $fqdn
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = clients::find($id);
        /**Obtenemos del cliente el hostname_id para relacionar con el Hostname*/
        $hostname = Hostname::find($client->hostname_id);
        /**Una vez obtenido el hostname obtenemos el website */
        $website = Website::find($hostname->website_id);
        /**Empezamos con la eliminación  de los datos*/
        app(HostnameRepository::class)->delete($hostname, true);
        app(WebsiteRepository::class)->delete($website, true);

        return [
            'success' => true,
            'message' => 'El cliente fue eliminado correctamente'
        ];
    }
    public function records()
    {

        $records = clients::latest()->get();
        return new ClientsCollections($records);
    }
    public function url_base()
    {


        $url_base = '.' . config('tenant.app_url_base');
        return compact('url_base');
    }
    public function validar_tenant($website, $subdominio)
    {
        $validar = $website::where('uuid', $subdominio)->first();
        if ($validar) {
            throw new Exception("El subdominio ya se encuentra registrado");
        }
    }
}
