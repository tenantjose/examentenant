<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Configuration extends Model
{
    use UsesTenantConnection;
    protected $fillable = ['send_auto', 'cron'];
}
