<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Traits\UsesSystemConnection;

class clients extends Model
{
    use UsesSystemConnection;

    protected $with = ['hostname'];
    protected $fillable = [
        'hostname_id',
        'documento',
        'name_customer',
        'email',
    ];

    public function hostname()
    {
        return $this->belongsTo(Hostname::class)->with(['website']);
    }
}
