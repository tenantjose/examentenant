<?php

    Route::get('/', 'System\LoginController@showLoginForm');
    Route::get('login', 'System\LoginController@showLoginForm')->name('login');
    Route::post('login', 'System\LoginController@login');
    Route::post('logout', 'System\LoginController@logout')->name('logout');

    Route::get('search', 'SearchController@index')->name('search.index');
    Route::get('buscar', 'SearchController@index')->name('search.index');
    Route::get('search/tables', 'SearchController@tables');

    Route::get('downloads/voided/{type}/{external_id}', 'VoidedController@downloadExternal')->name('voided.download_external');
    Route::get('downloads/{model}/{type}/{external_id}', 'DownloadController@downloadExternal')->name('download.external_id');

    Route::middleware('auth:admin')->group(function() {
        Route::get('/clients', function () {
            return view('clients.index');

        });
        Route::get('clients/records','System\ClientsController@records');
        Route::get('clients/url_base', 'System\ClientsController@url_base');
        Route::post('clients', 'System\ClientsController@store');
        Route::get('clients/records','System\ClientsController@records');
        Route::delete('clients/{client}', 'System\ClientsController@destroy');

    });
