<?php

return [
    'signature_note' => env('SIGNATURE_NOTE'),
    'signature_uri' => env('SIGNATURE_URI'),
    'items_per_page' => env('ITEMS_PER_PAGE', 20),
    'app_url_base' => env('APP_URL_BASE'),
];
